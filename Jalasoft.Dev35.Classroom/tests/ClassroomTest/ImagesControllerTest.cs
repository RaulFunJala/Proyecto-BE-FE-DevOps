﻿using System;
using System.Collections.Generic;
using System.Text;
using BussinessLogic;
using Classroom.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.Util;
using Moq;
using Xunit;

namespace ClassroomTest;

public class ImagesControllerTest
{
    private readonly ImagesController controller;
    private readonly Mock<IImagesService> serviceMock;
    private readonly Mock<IWebHostEnvironment> envMock;
    private readonly Mock<ISession> sesionMock;

    public ImagesControllerTest()
    {
        var inMemorySettings = new Dictionary<string, string>
        {
            { "FileSizeLimit", "524288" },
        };
        IConfiguration configuration = new ConfigurationBuilder()
            .AddInMemoryCollection(inMemorySettings)
            .Build();
        this.serviceMock = new Mock<IImagesService>();
        this.envMock = new Mock<IWebHostEnvironment>();
        this.sesionMock = new Mock<ISession>();
        this.controller = new ImagesController(this.envMock.Object, configuration, this.serviceMock.Object);
        this.controller.ControllerContext = new ControllerContext();
        this.controller.ControllerContext.HttpContext = new DefaultHttpContext();
        this.controller.HttpContext.Session = this.sesionMock.Object;
    }

    [Fact]
    public void Get_Returns_File()
    {
        this.envMock.Setup(x => x.WebRootPath).Returns(string.Empty);
        this.serviceMock.Setup(x => x.GetImage(It.IsAny<string>())).Returns(Encoding.UTF8.GetBytes("This is a dummy file"));
        Assert.IsType<FileContentResult>(this.controller.Get("country", "Bolivia.png"));
    }

    [Fact]
    public void Get_Returns_404NotFound()
    {
        this.envMock.Setup(x => x.WebRootPath).Returns(string.Empty);
        this.serviceMock.Setup(x => x.GetImage(It.IsAny<string>())).Returns<byte[]?>(null);
        Assert.IsType<NotFoundResult>(this.controller.Get("country", "Bolivia.png"));
    }

    [Fact]
    public void Get_Returns_500InternalError()
    {
        this.envMock.Setup(x => x.WebRootPath).Returns(string.Empty);
        this.serviceMock.Setup(x => x.GetImage(It.IsAny<string>())).Throws<Exception>();
        Assert.IsType<ObjectResult>(this.controller.Get("country", "Bolivia.png"));
    }

    [Fact]
    public void UploadImage_Returns_200Ok()
    {
        var file = new FileUpload();
        this.envMock.Setup(x => x.WebRootPath).Returns("/");
        this.serviceMock.Setup(x => x.SaveImage(It.IsAny<Image>())).Returns("imageUrl");
        var result = this.controller.UploadImage("country", "Bolivia", file);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void UploadImage_Returns_400BadRequest()
    {
        var file = new FileUpload();
        this.envMock.Setup(x => x.WebRootPath).Returns("/");
        this.serviceMock.Setup(x => x.SaveImage(It.IsAny<Image>())).Throws<InvalidInputException>();
        var result = this.controller.UploadImage("country", "Bolivia", file);
        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public void UploadImage_Returns_500InternalError()
    {
        var file = new FileUpload();
        this.envMock.Setup(x => x.WebRootPath).Returns("/");
        this.serviceMock.Setup(x => x.SaveImage(It.IsAny<Image>())).Throws<Exception>();
        var result = this.controller.UploadImage("country", "Bolivia", file);
        Assert.IsType<ObjectResult>(result);
    }
}
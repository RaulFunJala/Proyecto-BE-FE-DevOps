﻿using System;
using System.Collections.Generic;
using BussinessLogic;
using Classroom.Controllers;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Util;
using Models.Util.Enums;
using Moq;
using Xunit;
namespace ClassroomTest;

public class SubjectControllerTest
{
    private SubjectController controller;
    private Mock<ISubjectService> subjectServiceMock;

    public SubjectControllerTest()
    {
        this.subjectServiceMock = new Mock<ISubjectService>();
        this.controller = new SubjectController(this.subjectServiceMock.Object);
    }

    [Fact]
    public void Get_Returns_200_Ok()
    {
        this.subjectServiceMock.Setup(moq => moq.GetByCourse(It.IsAny<int>()))
            .Returns(new List<Subject> { new Subject(), new Subject() });
        Assert.IsType<OkObjectResult>(this.controller.Get(1));
    }

    [Fact]
    public void Get_Returns_204_NoContent()
    {
        this.subjectServiceMock.Setup(moq => moq.GetByCourse(It.IsAny<int>()))
            .Returns(new List<Subject>());
        Assert.IsType<NoContentResult>(this.controller.Get(1));
    }

    [Fact]
    public void Get_Returns_500_InternalError()
    {
        this.subjectServiceMock.Setup(moq => moq.GetByCourse(It.IsAny<int>()))
            .Throws<Exception>();
        Assert.IsType<StatusCodeResult>(this.controller.Get(1));
    }

    [Fact]
    public void Post_Returns_201_Created()
    {
        var newSubject = new Subject();
        newSubject.SubjectName = "Backend";
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        this.subjectServiceMock.Setup(moq => moq.Create(It.IsAny<Subject>()))
            .Returns(new Subject());
        Assert.IsType<CreatedResult>(this.controller.Post(newSubject));
    }

    [Fact]
    public void Post_Returns_400_BadRequest()
    {
        var newSubject = new Subject();
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        this.subjectServiceMock.Setup(moq => moq.Create(It.IsAny<Subject>()))
            .Throws<InvalidInputException>();
        Assert.IsType<BadRequestObjectResult>(this.controller.Post(newSubject));
    }

    [Fact]
    public void Post_Returns_500_InternalError()
    {
        var newSubject = new Subject();
        this.subjectServiceMock.Setup(moq => moq.Create(It.IsAny<Subject>()))
            .Throws<Exception>();
        Assert.IsType<StatusCodeResult>(this.controller.Post(newSubject));
    }
}
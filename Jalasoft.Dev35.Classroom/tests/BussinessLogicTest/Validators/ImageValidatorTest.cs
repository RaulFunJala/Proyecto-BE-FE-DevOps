﻿using System.IO;
using System.Text;
using BussinessLogic.Validators;
using FluentValidation.TestHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Models;
using Xunit;

namespace BussinessLogicTest.Validators;

public class ImageValidatorTest
{
    private readonly ImageValidator validator;

    public ImageValidatorTest()
    {
        this.validator = new ImageValidator();
    }

    [Fact]
    public void Should_Have_Error_When_Files_Is_Null()
    {
        IFormFile files = new FormFile(Stream.Null, 0, 0, string.Empty, string.Empty);
        var file = new FileUpload(files);
        var image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        var result = this.validator.TestValidate(image);
        result.ShouldHaveValidationErrorFor(img => img.File.Files);
    }

    [Fact]
    public void Should_Have_Error_When_Files_Is_Empty()
    {
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 0, "Data", "dummy.png");
        var file = new FileUpload(files);
        var image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        var result = this.validator.TestValidate(image);
        result.ShouldHaveValidationErrorFor(img => img.File.Files);
    }

    [Fact]
    public void Should_Have_Error_When_Files_Is_Too_Large()
    {
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 1001, "Data", "dummy.png");
        var file = new FileUpload(files);
        var image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        var result = this.validator.TestValidate(image);
        result.ShouldHaveValidationErrorFor(img => img);
    }

    [Fact]
    public void Should_Have_Error_When_Invalid_Extension()
    {
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 999, "Data", "dummy.txt");
        var file = new FileUpload(files);
        var image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        var result = this.validator.TestValidate(image);
        result.ShouldHaveValidationErrorFor(img => img.Extension);
    }

    [Theory]
    [InlineData(new object[] { "dummyFile.png" })]
    [InlineData(new object[] { "dummyFile.jpg" })]
    [InlineData(new object[] { "dummyFile.jpeg" })]
    public void Should_Not_Have_Any_Error(string fileName)
    {
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 999, "Data", fileName);
        var file = new FileUpload(files);
        var image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        var result = this.validator.TestValidate(image);
        result.ShouldNotHaveValidationErrorFor(img => img);
    }
}
﻿using System;
using BussinessLogic.Validators;
using FluentValidation.TestHelper;
using Models;
using Models.Util.Enums;
using Xunit;

namespace BussinessLogicTest.Validators;

public class SubjectValidatorTest
{
    private SubjectValidator validator;

    public SubjectValidatorTest()
    {
        this.validator = new SubjectValidator();
    }

    [Fact]
    public void Should_Have_Error_When_SubjectName_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.SubjectName);
    }

    [Fact]
    public void Should_Have_Error_When_SubjectName_Is_Empty()
    {
        var subject = new Subject();
        subject.SubjectName = string.Empty;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.SubjectName);
    }

    [Fact]
    public void Should_Have_Error_When_SubjectName_Is_Too_Long()
    {
        var subject = new Subject();
        subject.SubjectName = "Backend usando c# y dotnet framework para crear API's";
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.SubjectName);
    }

    [Fact]
    public void Should_Have_Error_When_StartingDate_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.StartingDate);
    }

    [Fact]
    public void Should_Have_Error_When_TrainerName_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.TrainerName);
    }

    [Fact]
    public void Should_Have_Error_When_TrainerName_Is_Empty()
    {
        var subject = new Subject();
        subject.TrainerName = string.Empty;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.TrainerName);
    }

    [Fact]
    public void Should_Have_Error_When_TrainerName_Is_Too_Long()
    {
        var subject = new Subject();
        subject.TrainerName = "Jose Benito V Pingüaramiricuaro de la Villa Segunda de San Pedro el Santo Padre";
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.TrainerName);
    }

    [Fact]
    public void Should_Have_Error_When_StartTime_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.StartTime);
    }

    [Fact]
    public void Should_Have_Error_When_StartTime_Is_Empty()
    {
        var subject = new Subject();
        subject.StartTime = string.Empty;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.StartTime);
    }

    [Fact]
    public void Should_Have_Error_When_StartTime_Is_Too_Long()
    {
        var subject = new Subject();
        subject.StartTime = "14:15.000";
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.StartTime);
    }

    [Fact]
    public void Should_Have_Error_When_EndTime_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.EndTime);
    }

    [Fact]
    public void Should_Have_Error_When_EndTime_Is_Empty()
    {
        var subject = new Subject();
        subject.EndTime = string.Empty;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.EndTime);
    }

    [Fact]
    public void Should_Have_Error_When_EndTime_Is_Too_Long()
    {
        var subject = new Subject();
        subject.EndTime = "18:30.000";
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.EndTime);
    }

    [Fact]
    public void Should_Have_Error_When_DaysOfWeek_Is_Set_To_No_Day()
    {
        var subject = new Subject();
        subject.DaysOfWeek = 0;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.DaysOfWeek);
    }

    [Theory]
    [InlineData(new object[] { (Day)32 })]
    [InlineData(new object[] { (Day)64 })]
    [InlineData(new object[] { (Day)48 })]
    [InlineData(new object[] { (Day)33 })]
    [InlineData(new object[] { (Day)40 })]
    public void Should_Have_Error_When_DaysOfWeek_Is_Set_To_A_No_Week_Day(Day daysOfWeek)
    {
        var subject = new Subject();
        subject.DaysOfWeek = daysOfWeek;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.DaysOfWeek);
    }

    [Theory]
    [InlineData(new object[] { Day.Monday })]
    [InlineData(new object[] { Day.Tuesday })]
    [InlineData(new object[] { Day.Wednesday })]
    [InlineData(new object[] { Day.Thursday })]
    [InlineData(new object[] { Day.Friday })]
    [InlineData(new object[] { Day.Monday | Day.Wednesday | Day.Friday })]
    [InlineData(new object[] { Day.Tuesday | Day.Thursday })]
    [InlineData(new object[] { Day.Monday | Day.Tuesday | Day.Wednesday | Day.Thursday | Day.Friday })]
    public void Should_Be_Ok_When_DaysOfWeek_Week_Days(Day daysOfWeek)
    {
        var subject = new Subject();
        subject.DaysOfWeek = daysOfWeek;
        var result = this.validator.TestValidate(subject);
        result.ShouldNotHaveValidationErrorFor(sub => sub.DaysOfWeek);
    }

    [Fact]
    public void Should_Have_Error_When_Image_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.Image);
    }

    [Fact]
    public void Should_Have_Error_When_Image_Is_Empty()
    {
        var subject = new Subject();
        subject.Image = string.Empty;
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.Image);
    }

    [Fact]
    public void Should_Have_Error_When_Image_Is_Too_Long()
    {
        var subject = new Subject();
        subject.Image = "https://miservidor_de_API_supermega_asombroso_que_me_permite_acceder_a_mis_recursos_Para_mostrar_cosas_impresionantes/api/Images/subject/1-Docker.png";
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.Image);
    }

    [Fact]
    public void Should_Have_Error_When_CourseId_Is_Null()
    {
        var subject = new Subject();
        var result = this.validator.TestValidate(subject);
        result.ShouldHaveValidationErrorFor(sub => sub.CourseId);
    }

    [Theory]
    [InlineData(new object[] { 1 })]
    [InlineData(new object[] { 2 })]
    [InlineData(new object[] { 1000 })]
    [InlineData(new object[] { 4 })]
    [InlineData(new object[] { 200 })]
    public void Should_Be_Ok_When_CourseId_Is_Supplied(int a)
    {
        var subject = new Subject();
        subject.CourseId = a;
        var result = this.validator.TestValidate(subject);
        result.ShouldNotHaveValidationErrorFor(sub => sub.CourseId);
    }

    [Fact]
    public void Should_Not_Have_Any_Error_When_Subject_Has_All_Fields_Correct()
    {
        var subject = new Subject();
        subject.SubjectName = "Backend";
        subject.StartingDate = DateTime.Today.AddMonths(1);
        subject.TrainerName = "Josue Lima";
        subject.StartTime = "16:30";
        subject.EndTime = "18:00";
        subject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        subject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        subject.CourseId = 1;
        var result = this.validator.TestValidate(subject);
        result.ShouldNotHaveAnyValidationErrors();
    }
}
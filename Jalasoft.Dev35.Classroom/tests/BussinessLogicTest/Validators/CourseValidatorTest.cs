﻿using System;
using BussinessLogic.Validators;
using FluentValidation.TestHelper;
using Models;
using Xunit;

namespace BussinessLogicTest.Validators;
public class CourseValidatorTest
{
    private CourseValidator validator;

    public CourseValidatorTest()
    {
        this.validator = new CourseValidator();
    }

    [Fact]
    public void Should_Have_Validation_For_Values()
    {
        var course = new Course();
        var result = this.validator.TestValidate(course);
        result.ShouldHaveValidationErrorFor(x => x.CourseName);
        result.ShouldHaveValidationErrorFor(x => x.Description);
        result.ShouldHaveValidationErrorFor(x => x.StartingDate);
        result.ShouldHaveValidationErrorFor(x => x.EndingDate);
        result.ShouldHaveValidationErrorFor(x => x.Image);
    }

    [Fact]
    public void Should_Not_Have_Validation_For_CreatedAt_UpdatedAt()
    {
        var course = new Course();
        var result = this.validator.TestValidate(course);
        result.ShouldNotHaveValidationErrorFor(x => x.CreatedAt);
        result.ShouldNotHaveValidationErrorFor(x => x.UpdatedAt);
    }

    [Fact]
    public void Should_Have_Error_When_CourseName_And_Description_Is_Too_Long()
    {
        var course = new Course();
        course.CourseName = "This is a really long name for a course, it must be rejected";
        course.Description = "This is a really long name for a course, it must be rejectedThis is a really long name for a course, it must be rejectedThis is a really long name for a course, it must be rejectedThis is a really long name for a course, it must be rejected";
        var result = this.validator.TestValidate(course);
        result.ShouldHaveValidationErrorFor(x => x.CourseName);
        result.ShouldHaveValidationErrorFor(x => x.Description);
    }

    [Fact]
    public void Should_Have_Error_When_EndingDate_Is_Before_Than_StartingDate()
    {
        var course = new Course();
        course.StartingDate = new DateTime(2022, 10, 10);
        course.EndingDate = new DateTime(2021, 10, 10);
        var result = this.validator.TestValidate(course);
        result.ShouldNotHaveValidationErrorFor(x => x.StartingDate);
        result.ShouldHaveValidationErrorFor(x => x.EndingDate);
    }

    [Fact]
    public void Should_Have_Error_When_CourseName_And_Description_Is_Null()
    {
        var course = new Course();
        course.CourseName = null;
        course.Description = null;
        var result = this.validator.TestValidate(course);
        result.ShouldHaveValidationErrorFor(x => x.CourseName);
        result.ShouldHaveValidationErrorFor(x => x.Description);
    }

    [Fact]
    public void Should_Not_Have_Error_When_Description_Is_Empty()
    {
        var course = new Course();
        course.Description = string.Empty;
        var result = this.validator.TestValidate(course);
        result.ShouldNotHaveValidationErrorFor(x => x.Description);
    }

    [Fact]
    public void Should_Have_Error_When_Image_Is_Too_Long()
    {
        var course = new Course();
        course.Image = "This is a really long link for a image, it must be rejectedThis is a really long name for a course, it must be rejectedThis is a really long name for a course, it must be rejectedThis is a really long name for a course, it must be rejectedit must be rejectedit must be rejectedit must be rejectedit must be rejected";
        var result = this.validator.TestValidate(course);
        result.ShouldHaveValidationErrorFor(x => x.Image);
    }

    [Fact]
    public void Should_Have_Error_When_Image_Is_Empty()
    {
        var course = new Course();
        course.Image = string.Empty;
        var result = this.validator.TestValidate(course);
        result.ShouldHaveValidationErrorFor(x => x.Image);
    }

    [Fact]
    public void Should_Not_Have_Any_Validation_Errors_When_Course_Data_Is_Correct()
    {
        var course = new Course();
        course.CourseName = "Dev23";
        course.Description = "This is a course";
        course.StartingDate = new DateTime(2022, 1, 10);
        course.EndingDate = new DateTime(2022, 6, 10);
        course.Image = "https://classroom-project.com/api/Images/course/Dev23.png";
        var result = this.validator.TestValidate(course);
        result.ShouldNotHaveAnyValidationErrors();
    }

    [Theory]
    [InlineData(new object[] { "Dev123", "", "2022-01-10", "2022-05-10", "default" })]
    [InlineData(new object[] { "Dev-Redux", "this is a shory ", "2022-05-10", "2022-11-10", "https://localhost:1234/image.png" })]
    [InlineData(new object[] { "React-bootcamp", "this is a shory ", "2022-05-10", "2022-11-10", "default" })]
    public void Should_DoSMT(string name, string desc, DateTime start, DateTime end, string img)
    {
        var course = new Course() { CourseName = name, Description = desc, StartingDate = start, EndingDate = end, Image = img };
        var result = this.validator.TestValidate(course);
        result.ShouldNotHaveAnyValidationErrors();
    }
}

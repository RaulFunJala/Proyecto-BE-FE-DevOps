﻿using System;
using System.Collections.Generic;
using BussinessLogic;
using DataAccess;
using Models;
using Models.Util;
using Moq;
using Xunit;

namespace BussinessLogicTest;

public class CourseServiceTest
{
    private readonly CourseService service;
    private readonly Mock<ICourseRepository> courseRepositoryMock;

    public CourseServiceTest()
    {
        this.courseRepositoryMock = new Mock<ICourseRepository>();
        this.service = new CourseService(this.courseRepositoryMock.Object);
    }

    [Fact]
    public void GetAllCourses_ReturnsListOfCourses()
    {
        this.courseRepositoryMock.Setup(moq => moq.GetAll()).Returns(new List<Course>());
        List<Course> courses = this.service.GetAll();

        // courses.Should().Equal(new List<Course>()); fluent validation
        Assert.Equal(new List<Course>(), courses);
    }

    [Fact]
    public void GetCourseById_ReturnsCourse()
    {
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        Course? course = this.service.Get(1);
        Assert.NotNull(course);
        Assert.IsType<Course>(course);
    }

    [Fact]
    public void GetCourseByNonExistanId_ReturnsNull()
    {
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Course>(null);
        Course? course = this.service.Get(1212120);
        Assert.Null(course);
    }

    [Fact]
    public void CreateCourse_ReturnCourse()
    {
        Course newCourse = new Course();
        newCourse.CourseName = "Dev35";
        newCourse.Description = "This is a desc";
        newCourse.StartingDate = new DateTime(2022, 1, 10);
        newCourse.EndingDate = new DateTime(2022, 9, 10);
        newCourse.Image = "https://classroom-app.com/image.png";
        this.courseRepositoryMock.Setup(moq => moq.Save(It.IsAny<Course>())).Returns(new Course());
        Course? course = this.service.Create(newCourse);
        Assert.NotNull(course);
        Assert.IsType<Course>(course);
    }

    [Fact]
    public void CreateCourse_ThrowsInvalidInputExpection()
    {
        Course newCourse = new Course();
        newCourse.CourseName = "Dev35 this is a long name for a course so it is not valid, ignore this, ignore this, ignore this";
        newCourse.Description = "Dev35 this is a long name for a course so it is not valid, ignore this, ignore this, ignore thisignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this";
        newCourse.StartingDate = new DateTime(2022, 1, 10);
        newCourse.EndingDate = new DateTime(2021, 1, 10);
        newCourse.Image = string.Empty;
        Assert.Throws<InvalidInputException>(() => this.service.Create(newCourse));
    }

    [Fact]
    public void UpdateCourse_ReturnCourse()
    {
        Course updatedCourse = new Course()
        {
            Id = 1,
            CourseName = "Dev35",
            Description = "This is a desc",
            StartingDate = new DateTime(2022, 1, 10),
            EndingDate = new DateTime(2022, 9, 10),
            Image = "https://classroom-app.com/image.png",
        };

        this.courseRepositoryMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Returns(updatedCourse);
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        Course? course = this.service.Update(1, updatedCourse);

        // Assert.Equal(true, course.Equals(updatedCourse));
        Assert.NotNull(course);
        Assert.IsType<Course>(course);
    }

    [Fact]
    public void UpdateCourse_ReturnNullNotExistingCourse()
    {
        Course updatedCourse = new Course()
        {
            Id = 1,
            CourseName = "Dev35",
            Description = "This is a desc",
            StartingDate = new DateTime(2022, 1, 10),
            EndingDate = new DateTime(2022, 9, 10),
            Image = "https://classroom-app.com/image.png",
        };

        this.courseRepositoryMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Returns(updatedCourse);
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Course>(null);
        Course? course = this.service.Update(1, updatedCourse);
        Assert.Null(course);
    }

    [Fact]
    public void UpdateCourse_ThrowsInvalidInputExpection()
    {
        Course updatedCourse = new Course();
        updatedCourse.CourseName = "Dev35 this is a long name for a course so it is not valid, ignore this, ignore this, ignore this";
        updatedCourse.Description = "Dev35 this is a long name for a course so it is not valid, ignore this, ignore this, ignore thisignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this ignore this, ignore this, ignore this";
        updatedCourse.StartingDate = new DateTime(2022, 1, 10);
        updatedCourse.EndingDate = new DateTime(2021, 1, 10);
        updatedCourse.Image = string.Empty;
        this.courseRepositoryMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Returns(updatedCourse);
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        Assert.Throws<InvalidInputException>(() => this.service.Update(1, updatedCourse));
    }

    [Fact]
    public void DeleteeCourse_ReturnsBoolTrue()
    {
        this.courseRepositoryMock.Setup(moq => moq.Delete(It.IsAny<int>())).Returns(true);
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        bool? result = this.service.Delete(1);
        Assert.True(result);
    }

    [Fact]
    public void DeleteCourse_ReturnsNullNonExistingCourse()
    {
        this.courseRepositoryMock.Setup(moq => moq.Delete(It.IsAny<int>())).Returns(true);
        this.courseRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Course>(null);
        bool? result = this.service.Delete(1);
        Assert.Null(result);
    }
}

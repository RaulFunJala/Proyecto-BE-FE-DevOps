﻿using System;
using System.IO;
using System.Text;
using BussinessLogic;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Models;
using Models.Util;
using Moq;
using Xunit;

namespace BussinessLogicTest;

public class ImagesServiceTest
{
    private readonly ImagesService service;
    private readonly Mock<IImagesRepository> repositoryMock;
    private readonly Mock<IFileWrapper> fileMock;
    private readonly Image image;

    public ImagesServiceTest()
    {
        this.repositoryMock = new Mock<IImagesRepository>();
        this.fileMock = new Mock<IFileWrapper>();
        this.service = new ImagesService(this.repositoryMock.Object, this.fileMock.Object);
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 999, "Data", "file.png");
        var file = new FileUpload(files);
        this.image = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
    }

    [Fact]
    public void SaveImage_ReturnsOK()
    {
        this.repositoryMock.Setup(moq => moq.SaveImage(It.IsAny<Image>())).Returns("localhost:7050/api/Images/subject/backend.png");
        Assert.IsType<string>(this.service.SaveImage(this.image));
    }

    [Fact]
    public void SaveImage_Throws_InvalidInputException()
    {
        var bytes = Encoding.UTF8.GetBytes("This is a dummy file");
        IFormFile files = new FormFile(new MemoryStream(bytes), 0, 0, "Data", "file.png");
        var file = new FileUpload(files);
        var newImage = new Image(file, "localhost:7050/api/Images", "wwwroot/uploads/images/", "subject", "backend", 1000);
        Assert.Throws<InvalidInputException>(() => this.service.SaveImage(newImage));
    }

    [Fact]
    public void SaveImage_Throws_Exception()
    {
        this.repositoryMock.Setup(moq => moq.SaveImage(It.IsAny<Image>())).Throws<Exception>();
        Assert.Throws<Exception>(() => this.service.SaveImage(this.image));
    }

    [Fact]
    public void GetImage_Returns_File()
    {
        this.repositoryMock.Setup(moq => moq.GetImage(It.IsAny<string>())).Returns(Encoding.UTF8.GetBytes("This is a dummy file"));
        this.fileMock.Setup(moq => moq.Exists(It.IsAny<string>())).Returns(true);
        Assert.IsType<byte[]>(this.service.GetImage("thisfile.png"));
    }

    [Fact]
    public void GetImage_Returns_Null()
    {
        this.fileMock.Setup(moq => moq.Exists(It.IsAny<string>())).Returns(false);
        Assert.Null(this.service.GetImage("thisfile.png"));
    }

    [Fact]
    public void GetImage_Throws_Exception()
    {
        this.fileMock.Setup(moq => moq.Exists(It.IsAny<string>())).Returns(true);
        this.repositoryMock.Setup(moq => moq.GetImage(It.IsAny<string>())).Throws<Exception>();
        Assert.Throws<Exception>(() => this.service.GetImage("thisFile.png"));
    }
}
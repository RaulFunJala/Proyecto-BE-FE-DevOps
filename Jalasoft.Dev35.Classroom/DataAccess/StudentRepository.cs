﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models;
using Models.Util;

namespace DataAccess
{
    public class StudentRepository : IStudentRepository
    {
        private readonly SQLConnection sqlConnection;
        public StudentRepository(SQLConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
        }

        public Student Get(int id)
        {
            using IDbConnection db = new SqlConnection(this.sqlConnection.ConnectionString);
            var storeProc = "spStudent_Get";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
            return db.QuerySingleOrDefault<Student>(storeProc, parameters, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Student> GetAll()
        {
            using IDbConnection db = new SqlConnection(this.sqlConnection.ConnectionString);
            var storeProc = "spStudent_GetAll";
            return db.Query<Student>(storeProc, commandType: CommandType.StoredProcedure).ToList();
        }

        public Student Create(Student student)
        {
            using IDbConnection db = new SqlConnection(this.sqlConnection.ConnectionString);
            var storeProc = "spStudent_Create";
            var parameters = new DynamicParameters();
            parameters.Add("@FirstName", student.FirstName, DbType.String, ParameterDirection.Input, Student.MaxFirstNameLength);
            parameters.Add("@LastName", student.LastName, DbType.String, ParameterDirection.Input, Student.MaxLastNameLength);
            parameters.Add("@Email", student.Email, DbType.String, ParameterDirection.Input, Student.MaxEmailLength);
            parameters.Add("@BirthDay", student.BirthDay, DbType.Date, ParameterDirection.Input);
            parameters.Add("@Image", student.Image, DbType.String, ParameterDirection.Input, Student.MaxImageLength);
            parameters.Add("@CountryId", student.CountryId, DbType.Int32, ParameterDirection.Input, int.MaxValue);
            return db.Query<Student>(storeProc, parameters, commandType: CommandType.StoredProcedure).First();
        }

        public Student Update(int id, int? courseId)
        {
            using IDbConnection db = new SqlConnection(this.sqlConnection.ConnectionString);
            var storeProc = "spStudent_Update";
            var parameters = new DynamicParameters();
            parameters.Add("@CourseId", courseId, DbType.Int32, ParameterDirection.Input, int.MaxValue);
            parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
            return db.Query<Student>(storeProc, parameters, commandType: CommandType.StoredProcedure).First();
        }
    }
}

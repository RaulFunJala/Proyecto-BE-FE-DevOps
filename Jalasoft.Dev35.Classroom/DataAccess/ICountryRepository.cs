using Models;

namespace DataAccess;

public interface ICountryRepository
{
    public IEnumerable<Country> GetAll();
}
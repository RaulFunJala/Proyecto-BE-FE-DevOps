using Models;

namespace DataAccess;

public interface IStudentRepository
{
    public Student Get(int id);
    public IEnumerable<Student> GetAll();
    public Student Create(Student student);
    public Student Update(int id, int? courseId);
}
using Models;

namespace DataAccess;

public class ImagesRepository : IImagesRepository
{
    public string SaveImage(Image image)
    {
        string path = $"{image.BasePath}/uploads/images/{image.Folder}/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        using FileStream fileStream = File.Create($"{path}{image.Name}{image.Extension}");
        image.File.Files!.CopyTo(fileStream);
        fileStream.Flush();
        return $"{image.BaseUrl}/{image.Folder}/{image.Name}{image.Extension}";
    }

    public byte[] GetImage(string fileName)
    {
        return File.ReadAllBytes(fileName);
    }
}
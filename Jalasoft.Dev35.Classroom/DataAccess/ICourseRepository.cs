using Models;

namespace DataAccess;

public interface ICourseRepository
{
    public List<Course> GetAll();
    public Course? Get(int id);
    public Course? Save(Course course);
    public Course Update(int id, Course course);
    public bool Delete(int id);
}
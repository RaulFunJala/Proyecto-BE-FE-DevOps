﻿using DataAccess;
using Models;

namespace BussinessLogic;

public class CountryService : ICountryService
{
    private readonly ICountryRepository countryRepository;
    public CountryService(ICountryRepository countryRepository)
    {
        this.countryRepository = countryRepository;
    }

    public IEnumerable<Country> GetAll()
    {
        return this.countryRepository.GetAll();
    }
}

﻿namespace BussinessLogic;

using BussinessLogic.Validators;
using DataAccess;
using Models;
using FluentValidation.Results;
using Models.Util;
using System.Collections.Generic;
using System.Text;

public class CourseService : ICourseService
{
    private readonly ICourseRepository courseRepository;
    public CourseService(ICourseRepository courseRepository)
    {
        this.courseRepository = courseRepository;
    }

    public List<Course> GetAll()
    {
        return this.courseRepository.GetAll();
    }

    public Course? Get(int id)
    {
        return this.courseRepository.Get(id);
    }

    public Course? Create(Course course)
    {
        CourseValidator validator = new CourseValidator();
        ValidationResult result = validator.Validate(course);
        if (!result.IsValid)
        {
            StringBuilder message = new StringBuilder();
            foreach (var item in result.Errors)
            {
                message.Append($"{item.ErrorMessage}\n");
            }

            throw new InvalidInputException(message.ToString());
        }

        return this.courseRepository.Save(course);
    }

    public Course? Update(int id, Course course)
    {
        CourseValidator validator = new CourseValidator();
        ValidationResult result = validator.Validate(course);
        if (!result.IsValid)
        {
            StringBuilder message = new StringBuilder();
            foreach (var item in result.Errors)
            {
                message.Append($"{item.ErrorMessage}\n");
            }

            throw new InvalidInputException(message.ToString());
        }

        Course? oldCourse = this.courseRepository.Get(id);
        if (oldCourse is null)
        {
            return null;
        }

        return this.courseRepository.Update(id, course);
    }

    public bool? Delete(int id)
    {
        Course? oldCourse = this.courseRepository.Get(id);
        if (oldCourse is null)
        {
            return null;
        }

        return this.courseRepository.Delete(id);
    }
}

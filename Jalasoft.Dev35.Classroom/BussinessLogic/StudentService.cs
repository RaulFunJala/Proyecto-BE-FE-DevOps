﻿using BussinessLogic.Validators;
using DataAccess;
using FluentValidation.Results;
using Models;
using Models.Util;
using Models.Util.Exceptions;

namespace BussinessLogic
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository studentRepository;
        private readonly ICourseService courseService;

        public StudentService(IStudentRepository studentRepository, ICourseService courseService)
        {
            this.studentRepository = studentRepository;
            this.courseService = courseService;
        }

        public IEnumerable<Student> GetAll(bool onlyNotRegistered)
        {
            List<Student> students = this.studentRepository.GetAll().ToList();
            if (onlyNotRegistered)
            {
                return students.Where(student => student.CourseId is null);
            }
            else
            {
                return students;
            }
        }

        public IEnumerable<Student>? GetByCourseId(int courseId)
        {
            if (this.courseService.Get(courseId) is null)
            {
                return null;
            }

            List<Student> students = this.studentRepository.GetAll().ToList();
            return students.Where(student => student.CourseId == courseId);
        }

        public Student Create(Student student)
        {
            StudentValidator validator = new StudentValidator();
            ValidationResult result = validator.Validate(student);
            if (result.Errors.Count > 0)
            {
                throw new InvalidInputException(result.Errors[0].ErrorMessage);
            }

            this.ValidateRepeatedEmail(student.Email);
            return this.studentRepository.Create(student);
        }

        public Student? Update(int id, int? courseId)
        {
            Student student = this.Get(id);
            if (student is null)
            {
                return null;
            }

            if (courseId is not null && this.courseService.Get((int)courseId) is null)
            {
                return null;
            }

            if (courseId is not null && student.CourseId is not null)
            {
                throw new InvalidCourseAssigmentException(id);
            }

            return this.studentRepository.Update(id, courseId);
        }

        private void ValidateRepeatedEmail(string? email)
        {
            List<Student> students = this.GetAll(false).ToList();
            if (students.Find(x => x.Email == email) is not null)
            {
                throw new NotUniqueException(nameof(Student), "email", email);
            }
        }

        private Student Get(int id)
        {
            return this.studentRepository.Get(id);
        }
    }
}

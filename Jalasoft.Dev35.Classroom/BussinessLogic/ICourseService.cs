using Models;

namespace BussinessLogic;

public interface ICourseService
{
    public List<Course> GetAll();
    public Course? Get(int id);
    public Course? Create(Course course);
    public Course? Update(int id, Course course);
    public bool? Delete(int id);
}
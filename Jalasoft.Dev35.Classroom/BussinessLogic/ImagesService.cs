using BussinessLogic.Validators;
using DataAccess;
using FluentValidation.Results;
using Models;
using Models.Util;

namespace BussinessLogic;

public class ImagesService : IImagesService
{
    private readonly IImagesRepository repository;
    private readonly IFileWrapper fileWrapper;
    public ImagesService(IImagesRepository repository, IFileWrapper fileWrapper)
    {
        this.repository = repository;
        this.fileWrapper = fileWrapper;
    }

    public string SaveImage(Image image)
    {
        ImageValidator validator = new ImageValidator();
        ValidationResult result = validator.Validate(image);
        if (result.Errors.Count > 0)
        {
            throw new InvalidInputException(result.Errors[0].ErrorMessage);
        }

        return this.repository.SaveImage(image);
    }

    public byte[]? GetImage(string fileName)
    {
        if (this.fileWrapper.Exists(fileName))
        {
            return this.repository.GetImage(fileName);
        }

        return null;
    }
}
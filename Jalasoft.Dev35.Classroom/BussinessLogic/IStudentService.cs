using Models;

namespace BussinessLogic;

public interface IStudentService
{
    public IEnumerable<Student> GetAll(bool onlyNotRegistered);
    public IEnumerable<Student>? GetByCourseId(int courseId);
    public Student Create(Student student);
    public Student? Update(int id, int? courseId);
}
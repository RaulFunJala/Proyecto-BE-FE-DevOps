using Models;

namespace BussinessLogic;

public interface ICountryService
{
    public IEnumerable<Country> GetAll();
}
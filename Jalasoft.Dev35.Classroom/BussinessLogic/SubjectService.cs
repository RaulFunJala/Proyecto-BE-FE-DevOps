﻿using BussinessLogic.Validators;
using DataAccess;
using FluentValidation.Results;
using Models;
using Models.Util;

namespace BussinessLogic;
public class SubjectService : ISubjectService
{
    private readonly ISubjectRepository repository;
    private readonly ICourseService courseService;
    public SubjectService(ISubjectRepository repository, ICourseService courseService)
    {
        this.repository = repository;
        this.courseService = courseService;
    }

    public List<Subject> GetByCourse(int courseId)
    {
        return this.repository.GetByCourse(courseId);
    }

    public Subject? Get(int id)
    {
        return this.repository.Get(id);
    }

    public Subject? Create(Subject newSubject)
    {
        SubjectValidator validator = new SubjectValidator();
        ValidationResult result = validator.Validate(newSubject);
        if (result.Errors.Count > 0)
        {
            throw new InvalidInputException(result.Errors[0].ErrorMessage);
        }

        if (this.courseService.Get(newSubject.CourseId ?? 0) is null)
        {
            throw new InvalidInputException("Course does not exists");
        }

        List<Subject> subjects = this.GetByCourse(newSubject.CourseId ?? 0);
        if (subjects.Any())
        {
            var query = subjects.Where((s) => s.SubjectName == newSubject.SubjectName);
            if (query.Any())
            {
                throw new InvalidInputException("Subject name already registered");
            }
        }

        return this.repository.Create(newSubject);
    }
}
﻿using FluentValidation;
using Models;

namespace BussinessLogic.Validators;
public class CourseValidator : AbstractValidator<Course>
{
    public CourseValidator()
    {
        this.RuleFor(course => course.CourseName)
            .NotNull()
            .NotEmpty()
            .MaximumLength(Course.MaxCourseNameLength)
            .WithMessage("The name of the course have to be less than 25 characters");

        this.RuleFor(course => course.Description)
            .NotNull()
            .MaximumLength(Course.MaxDescriptionLength)
            .WithMessage("The description of the course have to be less than 125 characters");

        this.RuleFor(course => course.StartingDate)
            .NotNull()
            .NotEmpty()
            .WithMessage("The starting date is required");

        this.RuleFor(course => course.EndingDate)
            .NotNull()
            .NotEmpty()
            .GreaterThan(course => course.StartingDate)
            .WithMessage("The ending date must be later that starting date");

        this.RuleFor(course => course.Image)
            .NotNull()
            .NotEmpty()
            .MaximumLength(Course.MaxImageLength)
            .WithMessage("The link of the image have to be less than 125 characters");
    }
}

﻿namespace Models.Util;

public class ClassroomException : Exception
{
    public ClassroomException(int code, string? message)
        : base(message)
    {
        this.Code = code;
    }

    public int Code { get; init; }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Util.Exceptions
{
    public class InvalidCourseAssigmentException : ClassroomException
    {
        public InvalidCourseAssigmentException(int id)
            : base(3, $"Student with id: {id} already has a course assigned.")
        {
        }
    }
}

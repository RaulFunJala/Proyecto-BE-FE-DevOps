﻿namespace Models;

using System;

public class Course
{
    public static readonly int MaxCourseNameLength = 25;
    public static readonly int MaxDescriptionLength = 125;
    public static readonly int MaxImageLength = 125;
    public int? Id { get; init; } = null;
    public string? CourseName { get; set; }
    public string? Description { get; set; }
    public DateTime? StartingDate { get; set; } = null;
    public DateTime? EndingDate { get; set; } = null;
    public string? Image { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}
﻿using Models.Util;

namespace Models
{
    public class Student
    {
        public static readonly int MaxFirstNameLength = 25;
        public static readonly int MaxLastNameLength = 25;
        public static readonly int MaxEmailLength = 50;
        public static readonly int MaxImageLength = 125;

        public int Id { get; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime? BirthDay { get; set; }
        public string? Image { get; set; }
        public int? CourseId { get; }
        public int CountryId { get; set; }
    }
}
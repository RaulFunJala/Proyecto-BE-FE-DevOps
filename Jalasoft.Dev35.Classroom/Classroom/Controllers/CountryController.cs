﻿using BussinessLogic;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Classroom.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CountryController : ControllerBase
{
    private readonly ICountryService countryService;
    public CountryController(ICountryService countryService)
    {
        this.countryService = countryService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Country>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult GetAll()
    {
        try
        {
            IEnumerable<Country> countries = this.countryService.GetAll();
            if (!countries.Any())
            {
                return this.NoContent();
            }

            return this.Ok(countries);
        }
        catch (Exception ex)
        {
            return this.StatusCode(500, ex.Message);
        }
    }
}

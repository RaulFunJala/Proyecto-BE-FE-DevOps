﻿using BussinessLogic;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Util;

namespace Classroom.Controllers;

[Route("api/[controller]")]
public class SubjectController : Controller
{
    private readonly ISubjectService subjectService;
    public SubjectController(ISubjectService subjectService)
    {
        this.subjectService = subjectService;
    }

    // GET: api/Subject/1
    [HttpGet("{courseId}")]
    [ProducesResponseType(typeof(List<Subject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Get(int courseId)
    {
        try
        {
            List<Subject> subjects = this.subjectService.GetByCourse(courseId);
            if (subjects.Any())
            {
                return this.Ok(subjects);
            }

            return this.NoContent();
        }
        catch
        {
            return this.StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    // POST api/Subject
    [HttpPost]
    [ProducesResponseType(typeof(Subject), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public IActionResult Post([FromBody] Subject newSubject)
    {
        try
        {
            Subject? subject = this.subjectService.Create(newSubject);

            return this.Created("Subject", subject);
        }
        catch (InvalidInputException ex)
        {
            return this.BadRequest(ex.Message);
        }
        catch
        {
            return this.StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
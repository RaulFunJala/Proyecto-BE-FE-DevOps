﻿namespace Classroom.Controllers;

using BussinessLogic;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Util;

[ApiController]
[Route("api/[controller]")]
public class CourseController : ControllerBase
{
    private readonly ICourseService courseService;
    public CourseController(ICourseService courseService)
    {
        this.courseService = courseService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(Course), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public IActionResult GetAll()
    {
        try
        {
            List<Course>? todos = this.courseService.GetAll();

            if (todos is null || !todos.Any())
            {
                return this.NoContent();
            }

            return this.Ok(todos);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(500, "Something went wrong.");
        }
    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(Course), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Get(int id)
    {
        try
        {
            Course? course = this.courseService.Get(id);

            if (course is null)
            {
                return this.NotFound();
            }

            return this.Ok(course);
        }
        catch (InvalidOperationException ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(400, "Sql exception.");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(500, "Something went wrong.");
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(Course), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Post([FromBody] Course course)
    {
        try
        {
            Course? result = this.courseService.Create(course);

            if (result is null)
            {
                return this.BadRequest();
            }

            return this.Created(nameof(this.Post), result);
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(400, ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(500, "Something went wrong.");
        }
    }

    [HttpPut("{id}")]
    [ProducesResponseType(typeof(Course), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Put(int id, [FromBody] Course course)
    {
        try
        {
            Course? result = this.courseService.Update(id, course);

            if (result is null)
            {
                return this.NotFound();
            }

            return this.Ok(result);
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(400, ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(500, "Something went wrong.");
        }
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Delete(int id)
    {
        try
        {
            bool? result = this.courseService.Delete(id);
            if (result is null)
            {
                return this.NotFound();
            }

            return this.NoContent();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            return this.StatusCode(500, "Something went wrong.");
        }
    }
}

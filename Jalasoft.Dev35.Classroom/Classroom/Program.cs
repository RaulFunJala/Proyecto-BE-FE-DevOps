﻿using BussinessLogic;
using DataAccess;
using Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

SQLConnection sqlSetup = builder.Configuration.GetSection("SQLConnection").Get<SQLConnection>();

builder.Services.AddScoped<ICourseService, CourseService>();
builder.Services.AddScoped<ICountryService, CountryService>();
builder.Services.AddScoped<IImagesService, ImagesService>();
builder.Services.AddScoped<IStudentService, StudentService>();
builder.Services.AddScoped<ISubjectService, SubjectService>();

builder.Services.AddScoped<ICourseRepository>(x => new CourseRepository(sqlSetup));
builder.Services.AddScoped<ICountryRepository>(x => new CountryRepository(sqlSetup));
builder.Services.AddScoped<IImagesRepository, ImagesRepository>();
builder.Services.AddScoped<IStudentRepository>(x => new StudentRepository(sqlSetup));
builder.Services.AddScoped<ISubjectRepository>(x => new SubjectRepository(sqlSetup));

builder.Services.AddScoped<IFileWrapper, FileWrapper>();

builder.Services.AddCors(p => p.AddDefaultPolicy(builder =>
{
    builder.WithOrigins("*").AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();